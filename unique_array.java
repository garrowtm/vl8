public class unique_array<T>
{
    /*
    Я не совсем понял суть задания. Изначально я хотел реализовать HashMap,
    но его истинная и правильная реализация достаточно трудоемкая задача.
    Если делать все в точности так, как описано в статьях, которые вы приложили,
    то тут придется делать и массив из односвязных списков, красно-черные деревья и кучу всего другого.
    В то же время, в статье про HashSet все описано слишком просто,
    хотя он тоже имеет свою более углубленную реализацию.
    Поэтому я решил пойти по легкому пути и считерить, сделав то,
    что описано в статье про HashSet, которую вы приложили (использовав свой ArrayList из прошлой работы).
    Да, это далеко не настоящая реализация, но раз уж вы ее берете как основу...
    Если вам действительно нужно увидеть, на что мы способны, то попросите, мы реализуем подлинный HashMap.
    Но повторюсь, это займет достаточно много времени. Спасибо за уделенное внимание
    */

    dynamic_array<node> table = new dynamic_array<>();

    private int hash(T key)
    {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    public boolean is_empty()
    {
        return table.size() == 0;
    }

    public boolean contains(T obj)
    {
        int obj_hash = hash(obj);
        for (int i = 0; i < table.size(); i++)
        {
            if (obj_hash == table.get(i).hash)
            {
                return true;
            }
        }

        return false;
    }

    public void add(T obj)
    {
        if (!contains(obj))
        {
            table.add(new node<T>(hash(obj), obj));
        }
    }

    public void remove(T obj)
    {
        for (int i = 0; i < table.size(); i++)
        {
            if (table.get(i) == obj)
            {
                table.remove(i);
                break;
            }
        }
    }

    public void clear()
    {
        table.clear();
    }

    public int size()
    {
        return table.size();
    }

    public T get(int index)
    {
        return (T) table.get(index).value;
    }

    public static void main(String[] args)
    {
        unique_array<String> cities = new unique_array<>();
        cities.add("Санкт-Петербург");
        cities.add("Москва");
        cities.add("Казань");
        cities.add("Нижний Новгород");
        cities.add("Сочи");

        cities.add("Москва");
        cities.add("Нижний Новгород");
        cities.add("Казань");

        for (int i = 0; i < cities.size(); i++)
        {
            System.out.println(cities.get(i));
        }
    }
}