public class node<V>
{
    int hash;
    V value;

    node(int hash, V value)
    {
        this.hash = hash;
        this.value = value;
    }
}